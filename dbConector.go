package dbConector

import (
	"encoding/xml"
	"fmt"
	"os"
)

type ExceptionSorter struct {
	ExceptionClass string `xml:"class-name,attr"`
}

type ValidConnection struct {
	ConnClass string `xml:"class-name,attr"`
}

type Validation struct {
	ValidConnection ValidConnection `xml:"ns1:valid-connection-checker"`
	ExceptionSorter ExceptionSorter `xml:"ns1:exception-sorter"`
}

type DataSource struct {
	Jta         bool       `xml:"jta,attr"`
	JndiName    string     `xml:"jndi-name,attr"`
	PoolName    string     `xml:"pool-name,attr"`
	Enabled     bool       `xml:"enabled,attr"`
	JavaContext bool       `xml:"use-java-context,attr"`
	Spy         bool       `xml:"spy,attr"`
	UseCcm      bool       `xml:"use-ccm,attr"`
	Connect     string     `xml:"ns1:datasource>ns1:connection-url"`
	Class       string     `xml:"ns1:datasource>ns1:driver-class"`
	Driver      string     `xml:"ns1:datasource>ns1:driver"`
	MinPool     int        `xml:"ns1:datasource>ns1:pool>min-pool-size"`
	MaxPool     int        `xml:"ns1:datasource>ns1:pool>max-pool-size"`
	SecUser     string     `xml:"ns1:datasource>ns1:security>ns1:user-name"`
	SecPw       string     `xml:"ns1:datasource>ns1:security>ns1:password"`
	Validation  Validation `xml:"ns1:datasource>ns1:validation"`
	Timeout     string     `xml:"ns1:datasource>ns1:timeout,omitempty"`
	Statement   string     `xml:"ns1:datasource>ns1:statement,omitempty"`
}

type DataSources struct {
	XmlName    string     `xml:"xmlns:xsi,attr"`
	XmlNs      string     `xml:"xmlns:ns1,attr"`
	SchemaLocn string     `xml:"xsi:schemaLocation,attr"`
	DataSource DataSource `xml:"ns1:datasource"`
}

func ConnectDb(court, instance, database, courtUser, courtPw, deployDir string) {
	connection := &DataSources{XmlName: "http://www.w3.org/2001/XMLSchema-instance",
		XmlNs:      "http://www.ironjacamar.org/doc/schema",
		SchemaLocn: "http://www.ironjacamar.org/doc/schema file:/Users/moscac/dev/JFinSys-2.0/JFinSysServer/src/main/resources/wildfly-8.0-ds.xsd"}

	connection.DataSource = DataSource{Jta: true,
		JndiName:    "jdbc/JFinSysServer_" + court + "_" + instance,
		PoolName:    "",
		Enabled:     true,
		JavaContext: true,
		Spy:         false,
		UseCcm:      true,
		Connect:     "jdbc:mysql://db:3306/" + database + "?autoReconnect=true",
		Class:       "com.mysql.jdbc.Driver",
		Driver:      "mysql-connector-java-5.1.9-bin.jar",
		MinPool:     3,
		MaxPool:     30,
		SecUser:     courtUser,
		SecPw:       courtPw}

	connection.DataSource.Validation.ValidConnection = ValidConnection{ConnClass: "org.jboss.resource.adapter.jdbc.vendor.MySQLValidConnectionChecker"}
	connection.DataSource.Validation.ExceptionSorter = ExceptionSorter{ExceptionClass: "org.jboss.resource.adapter.jdbc.Vendor.MySQLExceptionSorter"}

	enc := xml.NewEncoder(os.Stdout)
	enc.Indent(" ", "  ")
	if err := enc.Encode(connection); err != nil {
		fmt.Printf("error: %v\n", err)
	}
	return
}
